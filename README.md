# Digiscreen

Digiscreen est un fond d'écran interactif pour la classe en présence ou à distance. 

L'application est publiée sous licence GNU GPLv3.
Sauf les fontes Abril Fat Face, Orbitron et Material Icons (Apache License Version 2.0) et la fonte HKGrotesk (Sil Open Font Licence 1.1)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run serve
```

### Compilation et minification des fichiers
```
npm run build
```

### Fichier .env nécessaire à la racine avant compilation avec 2 variables
```
VUE_APP_GOOGLE_API_KEY (clé API Google pour Youtube)
VUE_APP_PIXABAY_API_KEY (clé API Pixabay)
```

### Démo
https://ladigitale.dev/digiscreen/
